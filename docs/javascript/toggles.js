Prism.highlightAll();

// var ExampleCodePanel = require('../vue/ExampleCodePanel.vue');
// Vue.component('example-code-panel', require('../vue/ExampleCodePanel.vue'));

var app = new Vue({
  el: '#toggles',
  data () {
    return {
      contentVisible: false
    }
  },
  // components: {
  //   'example-code-panel': import('../vue/ExampleCodePanel.vue')
  // },
  methods: {
    toggleContent () {
      //console.log('toggle content');
      this.contentVisible = !this.contentVisible;
      // if (this.contentVisible) this.$dispatch('content-visible', this);
    }
  }
})
