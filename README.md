# README #

This README includes the steps that are necessary to get the Helix User Interface Library up and running for development.

### What is this repository for? ###

HxUi is a user interface library for all Medical Director apps. The idea behind this UI library is for other developers to quickly and efficiently build application UI with all the UX/UI and brand principles baked in.

### How do I get set up? ###

1. Clone this repository
2. CD into the cloned repo
3. run `npm install`

### Available Npm Scripts ###
* **To build framework sass only** - `npm run build-hx-css`
* **To build doc sass only** - `npm run build-doc-css`
* **To watch for sass changes in both framework and doc** - `npm run watch-css`

### Install as NPM ##
(via HTTP) npm install git+https://medicaldirector.visualstudio.com/helix/_git/HxUi --save

(via SSH) npm install git+ssh://medicaldirector.visualstudio.com/helix/_git/HxUi --save


## Working with the HxUi documentation ##

### Install jekyll Gem ###
`sudo gem install jekyll bundler` from terminal

### Install missing gems ###
cd into the docs directory
Run `bundle install` from terminal


### Serve documentation ###
Run `npm run serve-doc` from terminal
View localhost:4000 in your preferred browser.

### Build documentation ###
Run `npm run build-doc` from terminal



